@extends('layout.application')

@section('content')
<head>
  <style>
    .col-md-13{
      box-shadow: 1px 2px 3px rgba(0,0,0,0.3);
  	}
  </style>
</head>
<div class="container text-center" id="myPage">

  <div class="page-header">
      <h1><i class="fa fa-briefcase"></i> Términos y Condiciones</h1>
  </div>

    <div class="row">

      <div class="col-md-13" >
      <div class="panel">
        <div class="panel-heading" style="text-align: left; padding-left: 30px; padding-left: 80px;"><h3> Entregas</h3>
        </div>

          <div class="panel-body" style=" padding-left: 50px; padding-right: 50px;">
            <div class="col-md-12"  style="padding-left: 30px; padding-right: -40px; text-align: justify;">
              <p>Nuestro almacén revisa detenidamente cada pedido antes de enviarlo.</p>

              <p>Hacemos todo lo que está a nuestro alcance para lograr que tu pedido llegue lo antes posible.</p>

              <p>Los pedidos con destino en Tlaxcala y Zona Metropolitana se entregarán hasta tres días hábiles posteriores a la realización de la compra. Para el resto del país, los pedidos pueden tardar entre 3 a 7 días hábiles o hasta 2 días en envío exprés.</p>

              <p>Toma en cuenta que las compras hechas durante días festivos (oficiales) o en fines de semana, se procesarán en el primer día laboral siguiente. Si no podemos entregar el pedido en dicho intervalo de tiempo, te notificaremos lo antes posible.</p>

              <p>No podemos cambiar ningún detalle de pedido (producto, talla, color, dirección) una vez que la orden ya ha sido procesada.</p>
            </div>          
          </div>
      </div>
    </div>


	<div class="col-md-13" >
      <div class="panel">
        <div class="panel-heading" style="text-align: left; padding-left: 30px; padding-left: 80px;"><h3> Seguimiento del pedido</h3>
        </div>

          <div class="panel-body" style=" padding-left: 50px; padding-right: 50px;">
            <div class="col-md-12"  style="padding-left: 30px; padding-right: -40px; text-align: justify;">
              <p>En cuanto tu pedido salga de nuestro almacén, te enviaremos un correo electrónico en el cual incluiremos en número de guía de tu pedido, así como una liga para que puedas consultar el progreso de tu pedido en el momento que lo desees.</p>

              <p>También puedes hacer el seguimiento del pedido indicando aquí el número de guía.</p>

              <p>Toma en cuenta que el número de guía se activará hasta que tu pedido salga del almacén.</p>

              <p>En caso de que necesites ayuda, no dudes en ponerte en contacto con nosotros.</p>
            </div>          
          </div>
      </div>
    </div>

	<div class="col-md-13" >
      <div class="panel">
        <div class="panel-heading" style="text-align: left; padding-left: 30px; padding-left: 80px;"><h3> Envíos dañados</h3>
        </div>

          <div class="panel-body" style=" padding-left: 50px; padding-right: 50px;">
            <div class="col-md-12"  style="padding-left: 30px; padding-right: -40px; text-align: justify;">
              <p>Todos los pedidos que enviamos desde nuestro almacén son inspeccionados meticulosamente.</p>
              <p>Si aún así, recibes un paquete dañado, te pedimos que:</p>

              <p><li>Llame al número de atención al cliente: 52 1 222 564 0227 disponible de lunes a domingo de 9:00 a 21:00 horas.</li></p>

			  <p>Antes de aceptar la entrega, inspecciona el embalaje y su contenido para detectar algún posible daño que pueda haber ocurrido durante el envío.</p>
			  <p>Es normal que el embalaje pueda presentar algunos desperfectos, pero si el daño es importante, te pedimos atentamente que:</p>

			  <p><li>Si el daño es menor, acepta el paquete pero escribe una nota que indiques el daño, fírmala y anota la fecha.</li></p>
			  <p><li>Si el embalaje está seriamente dañado, puedes rechazar el pedido. Si este es el caso, por favor contáctate con nosotros inmediatamente.</li></p>
			  <p>Cuando te pongas en contacto con nuestro servicio de atención al cliente, es importante que tengas a la mano la fecha del pedido, el nombre del artículo y el número de seguimiento (toda esta información se incluye en el correo electrónico de confirmación del envío) junto con la dirección de correo electrónico y el nombre de usuario.</p>
			  <p>Haremos lo posible para abonar el importe de compra tan pronto como recibamos el pedido en nuestro almacén.<p>
			  <p>El pedido se cancelará y podrás realizar un pedido nuevo.<p>
			  <p>Si detectas un daño tras aceptar la entrega, puedes devolver el paquete con los adhesivos del proveedor que se incluyen.</p>

            </div>          
          </div>
      </div>
    </div>

    <div class="col-md-13" >
      <div class="panel">
        <div class="panel-heading" style="text-align: left; padding-left: 30px; padding-left: 80px;"><h3> Centro de Atención a Clientes</h3>
        </div>

          <div class="panel-body" style=" padding-left: 50px; padding-right: 50px;">
            <div class="col-md-12"  style="padding-left: 30px; padding-right: -40px; text-align: justify;">
              <p>Nuestro equipo está a tu servicio para apoyarte con cualquier duda, crítica o sugerencia que tengas; solo escríbenos a consultoriosopticos_tlax@hotmail.com y con gusto te atenderemos.</p>

            </div>          
          </div>
      </div>
    </div>

    <div class="col-md-13" >
      <div class="panel">
        <div class="panel-heading" style="text-align: left; padding-left: 30px; padding-left: 80px;"><h3> Recuerda</h3>
        </div>

          <div class="panel-body" style=" padding-left: 50px; padding-right: 50px;">
            <div class="col-md-12"  style="padding-left: 30px; padding-right: -40px; text-align: justify;">
              <ul>
              	<li>Debes enviar tu producto en su empaque original o en un empaque distinto con las mismas dimensiones.</li>
				<li>La guía de devolución expira a los 5 días naturales de haberse generado.</li>
				<li>Anota tu número de guía para que puedas rastrear tu paquete en su trayecto a nuestro almacén.</li>
				<li>Si solicitaste cambio, te confirmaremos por correo el envío de tu nuevo producto cuando recibamos tu devolución.</li>
				<li>Debes utilizar tu cupón en su totalidad para evitar perder el remanente.</li>
				<li>Tienes 30 días naturales para utilizar tu cupón a partir de la fecha en que lo recibas.</li>
				<li>Puedes comprar un producto de mayor valor al de tu cupón y cubrir la diferencia por el método de pago que prefieras.</li>
              </ul>

            </div>          
          </div>
      </div>
    </div>

  </div>

</div> 				
@stop

