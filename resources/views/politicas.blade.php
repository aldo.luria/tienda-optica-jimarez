@extends('layout.application')

@section('content')

<head>
  <style>
    .col-md-13{
      box-shadow: 1px 2px 3px rgba(0,0,0,0.3);
  	}
  </style>
</head>
<div class="container text-center" id="myPage">

  <div class="page-header">
      <h1><i class="fa fa-lock"></i> Políticas de Privacidad</h1>
  </div>

    <div class="row">

      <div class="col-md-13" >
      <div class="panel">
        <div class="panel-heading" style="text-align: left; padding-left: 30px; padding-left: 80px;"><h3> Avisos de Privacidad</h3>
        </div>

          <div class="panel-body" style=" padding-left: 50px; padding-right: 50px;">
            <div class="col-md-12"  style="padding-left: 30px; padding-right: -40px; text-align: justify;">
              <p>En términos de lo previsto por la Ley Federal de Protección de Datos Personales en Posesión de Particulares (en lo sucesivo la “Ley”) y el Reglamento de la Ley Federal De Protección de Datos Personales en Posesión de Particulares (en lo sucesivo el “Reglamento”), se emite el presente Aviso de Privacidad en los siguientes términos:</p>
            </div>          
          </div>
      </div>
    </div>


	<div class="col-md-13" >
      <div class="panel">
        <div class="panel-heading" style="text-align: left; padding-left: 30px; padding-left: 80px;"><h3> Responsable de datos personales</h3>
        </div>

          <div class="panel-body" style=" padding-left: 50px; padding-right: 50px;">
            <div class="col-md-12"  style="padding-left: 30px; padding-right: -40px; text-align: justify;">
              <p>Consultorios Ópticos a.c., (en lo sucesivo la “Responsable”), con domicilio en Calle Lira y Ortega No. 20 Plaza El Fresno Local 6 y Local 7. Centro Tlaxcala, Tlax., es la responsable de los datos personales del “Titular” o “Particulares”, que le sean proporcionados mediante la página de internet (Dominio), o por cualquier otro medio, así como del uso, difusión, protección y tratamiento que se le da a los mismos, con los fines que se determinan en el presente Aviso.</p>
              <p>Destacando que V-KINGS (en lo sucesivo “VK”) con domicilio en Carretera a El Carmen Xalpatlahuaya S/N, Huamantla, Tlaxcala, ha sido contratada por la Responsable para el desarrollo de la página de internet (Dominio), quien no cuenta con facultades para usar o disponer de dicha información por cuenta propia, empero, sí al resguardo y a los fines indicados por la Responsable en el presente Aviso.</p>
            </div>          
          </div>
      </div>
    </div>

	<div class="col-md-13" >
      <div class="panel">
        <div class="panel-heading" style="text-align: left; padding-left: 30px; padding-left: 80px;"><h3> Manera de recabar datos personales</h3>
        </div>

          <div class="panel-body" style=" padding-left: 50px; padding-right: 50px;">
            <div class="col-md-12"  style="padding-left: 30px; padding-right: -40px; text-align: justify;">
              <p>Los Particulares proporcionarán sus datos personales a la Responsable, por medio de la página de Internet señalada en el punto inmediato anterior, al momento de registrarse para obtener un perfil en la misma, o bien, al momento de realizar la compra de algún producto en línea, simplemente para agilizar futuras consultas en el portal de conformidad con los productos que ha frecuentado o bien realizar compras, recibir publicidad y promociones, de cualquier tipo. Página en la cual proporcionarán, únicamente, los siguientes Datos Personales: </p>

Datos de identificación:

			<ul>
				<li>Nombre</li>
				<li>Apellido</li>
				<li>Correo electrónico</li>
				<li>Domicilio</li>

			</ul>

			<p>La Responsable o VK, según fuere el caso, no se hacen responsables por fallas técnicas en el sistema, por captura de datos o por información que el Titular incorrectamente proporcione, siendo la obligación y responsabilidad de éste, verificar que sus datos sean correctos y suficientes para los fines que pretenda.</p>

            </div>          
          </div>
      </div>
    </div>

    <div class="col-md-13" >
      <div class="panel">
        <div class="panel-heading" style="text-align: left; padding-left: 30px; padding-left: 80px;"><h3> CAMBIOS AL AVISO DE PRIVACIDAD</h3>
        </div>

          <div class="panel-body" style=" padding-left: 50px; padding-right: 50px;">
            <div class="col-md-12"  style="padding-left: 30px; padding-right: -40px; text-align: justify;">
              <p>La Responsable se reserva el derecho de efectuar en cualquier momento, modificaciones o actualizaciones al presente Aviso de Privacidad, para la atención de novedades legislativas, políticas internas o nuevos requerimientos para la prestación u ofrecimiento de nuestros productos y servicios. Cualquier cambio al presente, será informado a través nuestra página de internet.</p>
              <p>En caso de duda o aclaración relativa al contenido del presente Aviso de Privacidad, el contacto con la Responsable será a través del correo electrónico señalado.</p>
            </div>          
          </div>
      </div>
    </div>

  </div>

</div> 				
@stop
