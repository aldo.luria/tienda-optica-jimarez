<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
    .carousel-indicators li{
      border-color: #02288a;
      background-color: #19398b;
    }
    .carousel-indicators li:hover{
      border-color: #feb919;
      background-color: #f8c142;
    }
    .carousel-caption{
      color: #02288a;
    }
  </style>
</head>
<body>

<div class="container">
  <h2></h2>  
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
      <li data-target="#myCarousel" data-slide-to="4"></li>
      <li data-target="#myCarousel" data-slide-to="5"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="/images/banner1.jpg" alt="Consultorios Ópticos JimaRez" style="width:100%;">
        <div class="carousel-caption">
          <h3>¡Bienvenido!</h3>
        </div>
      </div>

      <div class="item">
        <img src="/images/banner6.jpg" alt="Examen" style="width:100%;">
        <div class="carousel-caption">
          <h3></h3>
          <p></p>
        </div>
      </div>

      <div class="item">
        <img src="/images/banner2.jpg" alt="Levi's" style="width:100%;">
        <div class="carousel-caption">
          <h3>Levi's</h3>
          <p>Busca y compra tus favoritos</p>
        </div>
      </div>
    
      <div class="item">
        <img src="/images/banner3.jpg" alt="Dockers" style="width:100%;">
        <div class="carousel-caption">
          <h3>Dockers</h3>
          <p>Busca y compra tus favoritos</p>
        </div>
      </div>

      <div class="item">
        <img src="/images/banner4.jpg" alt="La ink" style="width:100%;">
        <div class="carousel-caption">
          <h3>La ink</h3>
          <p>Busca y compra tus favoritos</p>
        </div>
      </div>

      <div class="item">
        <img src="/images/banner5.jpg" alt="Olive" style="width:100%;">
        <div class="carousel-caption">
          <h3>Olive</h3>
          <p>Busca y compra tus favoritos</p>
        </div>
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>

</body>
</html>
