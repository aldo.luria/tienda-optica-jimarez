<head>
	<style type="text/css">
	   .nav > li {
	    float:left;
		}
		.nav li ul {
		    display:none;
		    position:absolute;
		    min-width:140px;
		}   
		.nav li:hover > ul {
		    display:block;
		}   
		.nav li ul li {
		    position:relative;
		}   
		.nav li ul li ul {
		    left:-140px;
		    top:0px;
		    color: #fff;
		}

	</style>
</head>
<header>
	<nav class="navbar navbar-default navbar-fixed-top na-responsive">
		<div class="container">
			<div class="navbar-header">
				<a href="{{ route('welcome.index') }}"><img src="/images/logo_emp.png" style="width: 50px; height: 50px; float: left;"></a>
				<a href="{{ route('welcome.index') }}" class="navbar-brand main-title"> Ópticas JimaRez</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				
				<ul class="nav navbar-nav navbar-left">
					<li></li>
					<li>
					<form class="navbar-form navbar-left" role="search" action="{{url('home/searchredirect')}}">
				        <div class="input-group">
						    <input type="text" class="form-control" placeholder="Buscar" name="search" style="text-align: center; width: 300px;">
						    <div class="input-group-btn">
						      <button class="btn btn-default" type="submit">
						        <i class="fa fa-search"></i>
						      </button>
						    </div>
						  </div>
				    </form>
					</li>
					<!--<li>
						<form class="navbar-form navbar-left" role="search" action="{{url('home/searchredirect')}}">
							<div class="form-group">
								<input type="text" class="form-control" name='search' placeholder="Buscar ..." />
							</div>
                    <button type="submit" class="btn btn-default">Buscar</button></form>

					</li>-->
				</ul>

				<ul class="nav navbar-nav navbar-right">
				   
					<li><a href="{{ route('cart.show') }}"><i class="fa fa-shopping-cart"></i> Mi carrito</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							Productos
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
								<li><a href="{{ route('welcome.index') }}">Todos</a></li>
							@foreach ($categories as $category)
								<li><a href="{{ route('welcome.search.category', $category->slug) }}">{{ $category->name }}</a></li>
							@endforeach
						</ul>
					</li>

					<li><a href="{{ route('welcome.conocenos') }}">Conócenos</a></li>
					<li><a href="{{ route('welcome.ayuda') }}"><i class="fa fa-question"></i></a></li>
					<li><a href="#contacto"><i class="fa fa-phone"></i></a></li>

			@if(Auth::check())
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-user"></i> {{ Auth::user()->name }}
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							@if (Auth::user())
								<li>
									<a href="{{ route('users.logout') }}">Salir</a>
								</li>
								@include('partials.admin')
							@else
								<li>
									<a href="{{ route('users.login') }}">Iniciar Sesión</a>
								</li>
								<li>
									<a href="{{ route('users.register') }}">Registrarse</a>
								</li>
							@endif
						</ul>
					</li>
			@else
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-user"></i>
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							@if (Auth::user())
								<li>
									<a href="{{ route('users.logout') }}">Salir</a>
								</li>
								@include('partials.admin')
							@else
								<li>
									<a href="{{ route('users.login') }}">Iniciar Sesión</a>
								</li>
								<li>
									<a href="{{ route('users.register') }}">Registrarse</a>
								</li>
							@endif
						</ul>
					</li>
			@endif
				</ul>
			</div>
		</div>
	</nav>
</header>
