@extends('admin.template')

@section('content')
    <div class="container text-center">
        <div class="page-header">
            <h1>
                <i class="fa fa-cc-paypal"></i><br> PEDIDOS
            </h1>
        </div>
        
        <div class="page">
            
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">Ver Detalle</th>
                            <!--<th>Eliminar</th>-->
                            <th class="text-center">Fecha</th>
                            <th class="text-center">Usuario</th>
                            <th class="text-center">Dirección</th>
                            <th class="text-center">Correo</th>
                            <th class="text-center">Subtotal</th>
                            <th class="text-center">Envio</th>
                            <th class="text-center">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>
                                    <a 
                                        href="#" 
                                        class="btn btn-primary btn-detalle-pedido"
                                        data-id="{{ $order->id }}"
                                        data-path="{{ route('admin.orders.getItems') }}"
                                        data-toggle="modal" 
                                        data-target="#myModal"
                                        data-token="{{ csrf_token() }}"
                                    >
                                        <i class="fa fa-external-link"></i>
                                    </a>
                                </td>
                                <!--<td>
                                    {!! Form::open(['route' => ['admin.orders.destroy', $order->id]]) !!}
        								<button onClick="return confirm('Eliminar registro?')" class="btn btn-danger">
        									<i class="fa fa-trash-o"></i>
        								</button>
        							{!! Form::close() !!}
                                </td>-->
                                <td>{{ $order->created_at }}</td>
                                <td>{{ $order->user->name . " " . $order->user->last_name }}</td>
                                <td>{{ $order->user->address }}</td>
                                <td>{{ $order->user->email }}</td>
                                <td>${{ number_format($order->subtotal,2) }}</td>
                                <td>${{ number_format($order->shipping,2) }}</td>
                                <td>${{ number_format($order->subtotal + $order->shipping,2) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            
            
            <?php echo $orders->render(); ?>
            
        </div>
    </div>
    @include('admin.partials.modal-detalle-pedido')
@stop