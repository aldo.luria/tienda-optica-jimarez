@extends('admin.template')
@section('content')
	<div class="container text-center">
		<div class="page-header">
			<h1>
				<i class="fa fa-shopping-cart"></i>
				Nuevo Producto
			</h1>
		</div>
		<div class="row">
			<div class="col-md-offset-3 col-md-6">
				<div class="page">
					@include('admin.partials.errors')
					{!! Form::open(['route' => 'admin.products.store', 'method' => 'POST', 'files' => 'true']) !!}
						<div class="form-group">
							<label for="name">Nombre:</label>
							{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Ingresa el nombre...']) !!}
						</div>

						
						<div class="form-group">
							<label for="marca">Marca:</label>
							{!! Form::text('marca', null, ['class' => 'form-control','placeholder' => 'Ingresa la marca...']) !!}
						</div>
						<div class="form-group">
							<label for="color">Color:</label>
							{!! Form::text('color', null, ['class' => 'form-control','placeholder' => 'Ingresa el color...']) !!}
						</div>
						<div class="form-group">
							<label for="material">Material:</label>
							{!! Form::text('material', null, ['class' => 'form-control','placeholder' => 'Ingresa el matarial...']) !!}
						</div>


						<div class="form-group">
							<label for="name">Precio:</label>
							{!! Form::number('price', null, ['class' => 'form-control', 'placeholder' => 'Ingresa el precio...']) !!}
						</div>
						<div class="form-group">
							<label for="description">Descripcion:</label>
							{!! Form::textarea('description', null, ['class' => 'form-control']) !!}
						</div>
						<div class="form-group">
							<label for="extract">Pequeña Desc:</label>
							{!! Form::text('extract', null, ['class' => 'form-control']) !!}
						</div>
						<div class="form-group">
							<label for="name">Categoría: </label>
							{!! Form::select('category_id', $categories, ['class' => 'form-control']) !!}
						</div>
						<div class="form-group">
							<label for="image">Imagen:</label>
							{!! Form::file('image', ['class' => 'form-control']) !!}
						</div>
						<div class="form-group">
							<label for="quantity">Cantidad:</label>
							{!! Form::number('quantity',null,['class' => 'form-control', 'placeholder' => 'Ingresa la cantidad...']) !!}
						</div>

						<div class="form-group">
							<label for="visible">Visible: </label>
							<select name="visible">
                            <option value="1">Si</option>
                            <option value="0">No</option>
							{!! Form::select('visible') !!}
					      </select>
					      </div>

						<div class="form-group">
							<a href="{{ route('admin.products.index') }}" class="btn btn-danger">Cancelar</a>
							{!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@stop