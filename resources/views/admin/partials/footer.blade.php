<head>
	<style type="text/css">
		hr{
		    border-width:2px;
		    border-color:#19398b;
		    width:100%;
		}
		strong{
			color:#f8c142;
		}
		footer{
			color: white;
		}
		footer a{
			color: white;
		}
		
	</style>
</head>
<footer>

	<div class="row" style="width: 98%; float: left;">
		<div class="col-md-4 col-lg-3 col-xl-3">
			<h4 class="text-uppercase font-weight-bold">
                    <strong>Ópticas JimaRez</strong>
            </h4>
            <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
            <img id="logov" src="/images/logo_emp.png">
			<p style="text-align: justify;">En Ópticas JimaRez "Servicios de Salud Visual" podrás encontrar los mejores lentes y accesorios para ti y tu estilo. Asiste a nuestra sucursal y recibe un examen de la vista totalmente gratis. ¡Cómpralos ya!</p>
		</div>

		<div class="col-md-4 col-lg-3 col-xl-3" id="contacto" style="padding-left: 30px; padding-right: -40px; text-align: center;">
            <h4 class="text-uppercase font-weight-bold">
                <strong>Contacto</strong>
            </h4>
            <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
                <p><a href="{{ route('welcome.conocenos') }}#ubicacion"><i class="fa fa-home mr-3"></i> Calle Lira y Ortega No. 20 Plaza El Fresno Local 6 y Local 7. Centro Tlaxcala, Tlax.</a></p>
                <p><a href="mailto:consultoriosopticos_tlax@hotmail.com"><i class="fa fa-envelope mr-3"></i> consultoriosopticos_tlax@hotmail.com</a></p>
                <p><a href="tel:+5212225640227"><i class="fa fa-phone mr-3"></i> Óptica JimaRez: +52 1 222 564 0227</a></p>
                <p><a href=""><i class="fa fa-facebook-square mr-3"></i> Consultorios Ópticos</a></p>
                <p><a href=""><i class="fa fa-instagram mr-3"></i> Consultorios Ópticos</a></p>
    	</div>

		<div class="col-md-4 col-lg-3 col-xl-3">
			<h4 class="text-uppercase font-weight-bold">
                    <strong>Conócenos</strong>
            </h4>
	        <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
					<p><a href="{{ route('welcome.conocenos') }}#mision"> Misión </a></p>
	            	<p><a href="{{ route('welcome.conocenos') }}#vision"> Visión </a></p>
	                <p><a href="{{ route('welcome.conocenos') }}#objetivo"> Objetivo </a></p>	        
		</div>

        <div class="col-md-4 col-lg-3 col-xl-3">
			<h4 class="text-uppercase font-weight-bold">
                    <strong>Desarrollado por:</strong>
            </h4>
            <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
			<div class="auto-info">
				<img id="logov" src="/images/v-kings.png">
				<p style="text-align: justify;"><a href="mailto:aldo.luria@gmail.com">V-KING</a> es un equipo de trabajo conformado por estudiantes de la Universidad Tecnológica de Tlaxcala de la carrera Ingeniería en Tecnologías de la Información Área Multimedia y Comercio Electrónico.</p>
			</div>
		</div>
	</div>
	<hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 96%; float: left;">


	<div class="row container text-center">
      
	        <div class="auto-info">     
	            <p style="color:gray;"><i class="fa fa-copyright"></i> 2018 Copyright: <a href="mailto:aldo.luria@gmail.com">V-KINGS</a></p>
			</div>
 
    </div>



</footer>