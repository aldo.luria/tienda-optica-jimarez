<head>
	<style type="text/css">
	   .nav > li {
	    float:left;
		}
		.nav li ul {
		    display:none;
		    position:absolute;
		    min-width:140px;
		}   
		.nav li:hover > ul {
		    display:block;
		}   
		.nav li ul li {
		    position:relative;
		}   
		.nav li ul li ul {
		    left:-140px;
		    top:0px;
		    color: #fff;
		}
	</style>
</head>
<header>
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a href="{{ route('welcome.index') }}"><img src="/images/logo_emp.png" style="width: 50px; height: 50px; float: left;"></a>
				<a href="{{ route('welcome.index') }}" class="navbar-brand main-title"> Ópticas JimaRez</a>
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">

					<li>
						<a href="/admin/home">Panle de Aministración</a>
					</li>

					<li><a href="{{ route('admin.categories.index') }}">Categorías</a></li>
					<li><a href="{{ route('admin.products.index') }}">Productos</a></li>
					<li><a href="{{ route('admin.users.index') }}">Usuarios</a></li>
					<li><a href="{{ route('admin.orders.index') }}">Pedidos</a></li>

					<!-- Menú desplegable
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							Categorías
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
								<li><a href="{{ route('admin.categories.index') }}">Todas</a></li>
								<li><a href="{{ route('admin.categories.create') }}">Crear</a></li>
						</ul>
					</li>

					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							Productos
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
								<li><a href="{{ route('admin.products.index') }}">Todos</a></li>
								<li><a href="{{ route('admin.products.create') }}">Crear</a></li>
						</ul>
					</li>

					<li><a href="{{ route('admin.orders.index') }}">Pedidos</a></li>

					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							Usuarios
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
								<li><a href="{{ route('admin.users.index') }}">Todos</a></li>
								<li><a href="{{ route('admin.users.create') }}">Crear</a></li>
						</ul>
					</li>

					-->

					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-user"></i> {{ Auth::user()->name }}
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a href="{{ route('users.logout') }}">Salir</a>
							</li>
							<li><a href="{{ route('welcome.index') }}">Ir a la tienda</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</header>