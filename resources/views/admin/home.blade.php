@extends('admin.template')

@section('content')

    <div class="container text-center">
        <div class="page-header">
            <h1><i class="fa fa-desktop"></i> 
                <br>Panel de Administración</h1>
        </div>
        
     
        
        <div class="row">
            
            <div class="col-md-3">
                <div class="panel">
                    <a href="{{ route('admin.categories.index') }}">
                    <p style="color: black">
                    <i class="fa fa-list-alt icon-home"></i>
                    <br>Crear, ver, editar y eliminar las categorías existentes en la tienda.</p>
                    </a>
                    <a href="{{ route('admin.categories.index') }}" class="btn label-success btn-block btn-home-admin">CATEGORÍAS</a>
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="panel">
                    <a href="{{ route('admin.products.index') }}">
                    <p style="color: black">
                    <i class="fa fa-shopping-cart  icon-home"></i>
                    <br>Crear, ver, editar y eliminar los productos existentes en la tienda.</p>
                    </a>
                    <a href="{{ route('admin.products.index') }}" class="btn label-success btn-block btn-home-admin">PRODUCTOS</a>
                </div>
            </div>

            <div class="col-md-3">
                <div class="panel">
                    <a href="{{ route('admin.users.index') }}">
                    <p style="color: black">
                    <i class="fa fa-users  icon-home"></i>
                    <br>Crear, ver, editar y eliminar los usuarios existentes en la tienda.</p>
                    </a>
                    <a href="{{ route('admin.users.index') }}" class="btn label-success btn-block btn-home-admin">USUARIOS</a>
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="panel">
                    <a href="{{ route('admin.orders.index') }}">
                    <p style="color: black">
                    <i class="fa fa-cc-paypal  icon-home"></i>
                    <br>Ver los pedidos y sus detalles, que los clientes han hechos a la tienda.</p>
                    </a>
                    <a href="{{ route('admin.orders.index') }}" class="btn label-success btn-block btn-home-admin">PEDIDOS</a>
                </div>
            </div> 
                    
        </div>
        
    </div>
    

@stop