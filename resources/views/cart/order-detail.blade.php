@extends('layout.application')

@section('content')
	<div class="container text-center">
		<div class="page-header">
			<h1><i class="fa fa-shopping-cart"></i> Detalle del pedido </h1>
		</div>
		<div class="page table-cart">
			<div class="table-responsive">
				<h3>Datos del usuario</h3>
				<table class="table table-striped table-hover table-bordered">
					<tr><th class="text-center">Nombre: </th><td>{{ Auth::user()->name . ' ' . Auth::user()->last_name }}</td></tr>
					<tr><th class="text-center">Usuario: </th><td>{{ Auth::user()->username }}</td></tr>
					<tr><th class="text-center">Correo: </th><td>{{ Auth::user()->email }}</td></tr>
					<tr><th class="text-center">Dirección: </th><td>{{ Auth::user()->address }}</td></tr>
				</table>
			</div>
			<div class="table-responsive text-center">
				<h3>Datos del pedido</h3>
				<table class="table table-striped table-hover table-bordered">
					<tr>
						<th class="text-center">Producto</th>
						<th class="text-center">Precio</th>
						<th class="text-center">Cantidad</th>
						<th class="text-center">Subtotal</th>
					</tr>
					@foreach ($cart as $item)
						<tr>
							<td>{{ $item->name }}</td>
							<td>{{ number_format($item->price, 2) }}</td>
							<td>{{ $item->quantity }}</td>
							<td>{{ number_format($item->price * $item->quantity, 2) }}</td>
						</tr>
					@endforeach

					<!-- Aquí se muestra el IVA que la Viejita pidió >:v -->
					<tr>
							<td>IVA incluido</td>
							<td>{{ number_format(($total / 116) * 16, 2) }}</td>
							<td>1</td>
							<td>{{ number_format(($total / 116) * 16, 2) }}</td>
					</tr>

					<!-- Aquí se muestra el envio el la tabla-->
					<tr>
							<td>Envío</td>
							<td>{{ number_format(100, 2) }}</td>
							<td>1</td>
							<td>{{ number_format(100, 2) }}</td>
					</tr>
					

				</table><br>
				<h3>
					<span class="label label-success">
						Total: ${{ number_format(100 + $total , 2)}}
					</span>
				</h3>
				<br>

				<br>
				<p>
					<a href="{{ route('cart.show') }}" class="btn btn-primary">
						<i class="fa fa-chevron-circle-left fa-2x"></i> Regresar
					</a>
					<a href="{{ route('payment') }}" class="btn btn-warning">
						Pagar con <i class="fa fa-cc-paypal fa-2x"></i>
					</a>
					<br><br>
					<a href="{{ route('welcome.ayuda') }}#mandar-receta" target="_blank"> ¿Cómo puedo mandar mi receta? </a>
				</p>
			</div>
		</div>
		
	</div>
@stop