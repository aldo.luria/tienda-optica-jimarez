
@extends('layout.application')

@section('content')
<head>
	<style type="text/css">
		.panel{
			padding: 5px;
			padding-right: 10px;
			padding-left: 10px;
			min-height: 180px;
			max-height: 100%;
			box-shadow: 0px 1px 2px rgba(0,0,0,0.3);
		}
		.col-md-4 .panel:hover {
			box-shadow: 1px 1px 10px rgba(0,0,0,0.5);
			margin-top: -5px;
			-webkit-transition: all 0.3s ease-in-out;
			-moz-transition: all 0.3s ease-in-out;
			-o-transition: all 0.3s ease-in-out;
			transition: all 0.3s ease-in-out;
		}
	</style>

</head>
<div class="container text-center">

	<div class="page-header">
			<h1><i class="fa fa-info-circle"></i> Conócenos</h1>
	</div>

	<div class="col-md-13	">
		<div class="panel">
			<img src="images/logo_op.png">
		</div>
	</div>


<!--
<div class="row">
	
	<div class="col-md-4 white-panel2" style="margin-left: 7.5px; margin-right: 7.5px; margin-top: 10px; margin-bottom: 7px;">
		<h2 id="mision">Misión</h2>
		<p>Ofrecer a todos nuestros pacientes un servicio de salud visual y la corrección de sus problemas refractivos, pues como sabemos, una buena visión nos ofrecerá una mejor calidad de vida.</p>
	</div>
	
	<div class="col-md-4 white-panel2" style="margin-left: 7.5px; margin-right: 7.5px; margin-top: 10px; margin-bottom: 7px;">
		<h2 id="vision">Visión</h2>
		<p>Ofrecer un servicio de calidad total y consolidarnos como la primera opción en nuestros pacientes al solicitar un servicio de salud visual.</p>
	</div>

	<div class="col-md-4 white-panel2" style="margin-left: 7.5px; margin-right: 7.5px; margin-top: 10px; margin-bottom: 7px;">	
		<h2 id="objetivo">Objetivo</h2>
		<p>Servicio de la salud visual, hacer jornadas visuales, lentes a accesible costo, el examen de la vista gratis.</p>

	</div>

</div>
-->

<div class="row">
	
	<div class="col-md-4">
		<div class="panel">
			<h2 id="mision">Misión</h2>
			<p>Ofrecer a todos nuestros pacientes un servicio de salud visual y la corrección de sus problemas refractivos, pues como sabemos, una buena visión nos ofrecerá una mejor calidad de vida.</p>
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="panel">
			<h2 id="vision">Visión</h2>
			<p>Ofrecer un servicio de calidad total y consolidarnos como la primera opción en nuestros pacientes al solicitar un servicio de salud visual.</p>
		</div>
	</div>

	<div class="col-md-4">
		<div class="panel">
			<h2 id="objetivo">Objetivo</h2>
			<p>Servicio de la salud visual, hacer jornadas visuales, lentes a accesible costo, el examen de la vista gratis.</p>
		</div>
	</div>

</div>


	<div class="col-md-13">
		<div class="panel">
			<h2 id="ubicacion">Ubicación</h2>
		<p>Calle Lira y Ortega No. 20 Plaza El Fresno<b> Local 6 y Local 7</b>. Centro Tlaxcala, Tlax.</p>
		<div id="mapa">
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2075.291593416507!2d-98.23707085074517!3d19.32140433704849!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cfd93b69430ea7%3A0x6b57ffc6ff355c31!2sPlaza+El+Fresno!5e0!3m2!1ses-419!2smx!4v1521966062376" width="100%" height="500" frameborder="0" style="border:0; border-radius:10px;" allowfullscreen></iframe>
		</div>
		</div>
	</div>

		
<br>
<br>
</div>				
@stop

