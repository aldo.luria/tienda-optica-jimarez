@extends('layout.application')

@section('content')

<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    .col-md-13{
      box-shadow: 1px 2px 3px rgba(0,0,0,0.3);
    }
    img{
      border-radius:10px;
    }
    .glyphicon {
      font-size: 20px;
      margin-bottom: 20px;
  }
  h1{
    color: #19398b;
  }

  </style>
  <script>
$(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".container a[href='#myPage']").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });
})
</script>
</head>
<body>
<div class="container text-center" id="myPage">

  <div class="page-header">
      <h1><i class="fa fa-question"></i> Ayuda</h1>
  </div>

    <div class="row">
        <div class="col-md-13">
            <div class="panel panel-primary ">
                <div class="panel-heading" style="background-color: #19398b;"><h5>Índice</h5></div>
                <div class="panel-body" style=" padding-left: 50px; padding-right: 50px;">

                  <div class="col-md-6"  style="padding-left: 30px; padding-right: -40px; text-align: left;">
                    <p><a href="#lentes-de-plastico-o-cristal"><i class="fa fa-caret-right"></i> ¿Lentes de plástico o cristal?</a></p>
                    <p><a href="#proteccion-visual"><i class="fa fa-caret-right"></i> Protección visual en los lentes</a></p>
                    <p><a href="#materiales-de-armazon"><i class="fa fa-caret-right"></i> Materiales de armazón</a></p>
                  </div>
                            
                  <div class="col-md-6"  style="padding-left: 30px; padding-right: -40px; text-align: left;">
                    <p><a href="#mandar-receta"><i class="fa fa-caret-right"></i> ¿Cómo puedo mandar mi receta?</a></p>
                    <p><a href="#garantia-armazones"><i class="fa fa-caret-right"></i> ¿Tienen garantía mis armazones?</a></p>
                    <p><a href="#como-comprar-en-tienda"><i class="fa fa-caret-right"></i> ¿Cómo comprar en la tienda en línea?</a></p>
                  </div>

                </div>
            </div>
        </div>

        <hr id="lentes-de-plastico-o-cristal">
      <div class="col-md-13" >
      <div class="panel">
        <div class="panel-heading" style="text-align: left; padding-left: 30px; padding-left: 80px;"><h3> ¿Lentes de plástico o cristal?</h3>
        </div>
          <div class="panel-body" style=" padding-left: 50px; padding-right: 50px;">
            <div class="col-md-6"  style="padding-left: 30px; padding-right: -40px; text-align: justify;">
              <p>Esta pregunta es fundamental a la hora de elegir un par de gafas: ¿prefiere lentes de plástico o de cristal? Al tomar una decisión, recuerde que sus gafas deben ser fuertes, atractivas, resistentes a los golpes, cómodas y, por último pero no menos importante, fáciles de llevar. Sean cuales sean sus preferencias, tenga en cuenta lo siguiente: que un material sea adecuado depende de factores individuales como la agudeza visual y el gusto personal.</p>

              <p>Los lentes correctivos de cristal, es decir, los lentes fabricados en vidrio natural de acuerdo con su clasificación profesional, son la opción más común. Conservan su sitio en la optometría actual gracias a su excepcional resistencia a las ralladuras. Los consumidores agradecen el hecho de que sean más económicos que sus análogos de plástico. En casos de ametropía severa, los lentes de cristal pueden proporcionar la corrección necesaria sin dejar de ser delgados, un detalle estético que no debe subestimarse.</p>

              <p>El vidrio natural se recomienda también en el caso de los lentes bifocales o trifocales, ya que pueden mezclarse varios materiales sin que el borde resulte llamativo. En principio, el aumento del grosor del material hace que el lente parezca más pura; está más limpia y carece de molestas franjas de color (la llamada dispersión).</p>

              <p>Cuando la luz atraviesa un lente correctivo, se rompe y se desintegra. Esto crea un molesto espectro de color visible, similar a un prisma. La intensidad de este efecto, conocido como dispersión, depende de la calidad del material usado: material de alta calidad = dispersión mínima. El efecto franja de color se mide en función del llamado número de Abbé: Cuanto mayor sea el número de Abbé del material de un lente correctivo, menor será la dispersión. La ventaja del vidrio natural: produce franjas de color considerablemente más leves, incluso cuando el índice de refracción es idéntico al de los lentes de plástico.</p>

              <p>Cuanto mayor sea el rango del índice refractivo (también llamado índice de refracción) del material del lente correctivo, más delgado será el cristal acabado. En caso de elevadas dioptrías, es recomendable usar materiales con un índice refractivo bajo, ya que reducirán el grosor de los lentes y, consecuentemente, el peso de las gafas. Por ejemplo: un lente con un índice de refracción de 1,6 siempre es más delgado que un lente con un índice de refracción de 1,5, tratándose de la mismas dioptrías.</p>

              <p>El vidrio natural ofrece una ventaja clara: su índice refractivo comprende un rango de 1,5 a 1,9, mientras que el índice refractivo del vidrio orgánico (= plástico) abarca un reducido rango de 1,5 a 1,74. La densidad del vidrio natural también es superior a la del plástico. El resultado: incluso con el mismo índice refractivo, los lentes correctivos fabricados en vidrio siempre son más finas que los de plástico, aunque también bastante más pesados.</p>
            </div>
                  
            <div class="col-md-6"  style="padding-left: 30px; padding-right: -40px; text-align: justify;">
              <img src="/images/LPOC.jpg"><p></p>
              <p><h4>Lentes de plástico: ¿solo para los niños y los atletas?</h4></p>
              <p>Los lentes de plástico, también conocidas como lentes orgánicos, se utilizan en la actualidad para todo tipo de gafas, y son especialmente adecuados para los niños y los deportistas. Son muy ligeros y cómodas de llevar.</p>

              <p>También son especialmente resistentes a los golpes. En este sentido, son hasta 100 veces mejores que los lentes de cristal, dependiendo del tipo de plástico utilizado. Además, proporcionan una mejor protección contra las chispas (de los fuegos artificiales, las hogueras o los trabajos de soldadura o esmerilado, por ejemplo) y no se astillan. Esto supone una importante ventaja para la seguridad en muchas situaciones diarias.
              La desventaja: en comparación con los lentes de vidrio natural, los lentes de plástico no ofrecen una gran resistencia a las ralladuras. Por este motivo, son más sensibles y requieren mantenimiento adicional. Para contrarrestar esto, es posible aplicar un tratamiento especial que repela la suciedad o endurezca el material (por ejemplo, LotuTec de Carl Zeiss).
              Otra ventaja del plástico: mientras los lentes de vidrio natural solo pueden tintarse en algunos colores y a un precio bastante elevado, los lentes de plástico son fáciles de colorear en prácticamente todos los tonos. El plástico es la primera opción para aquellos que prefieran unas gafas con lentes coloreados como accesorio de moda.</p>   
            <br>
            <br>
            <p style="text-align: right;">Fuente: <a href="https://www.zeiss.co/vision-care/es_co/better-vision/entender-la-vision/lentes-y-soluciones/lentes-de-cristal-o-de-plastico.html" target="_blank">ZEISS</a></p>
            </div>
          </div>

            <a href="#myPage" title="Índice">
              <span class="glyphicon glyphicon-chevron-up"></span>
            </a>

      </div>
    </div>

    <hr  id="proteccion-visual">
    <div class="col-md-13">
      <div class="panel">
        <div class="panel-heading" style="text-align: left; padding-left: 30px; padding-left: 80px;"><h3> Protección visual en los lentes</h3></div>
                <div class="panel-body" style=" padding-left: 50px; padding-right: 50px;">
          <div class="col-md-4"  style="padding-left: 30px; padding-right: -40px; text-align: justify;">
                <p><h4 style="text-align: center;"><b>Anti-reflejante</b></h4>
                <img src="/images/A-R.jpg"></p>
                <p>Las lentes anti-reflejantes son lentes que eliminan los reflejos de luz que dificultan la visión.</p>

            <p>En las lentes, la acción de la luz (natural o artificial) provoca deslumbramientos y reflejos que resultan incómodos para nuestros ojos y dificultan la visión. Las lentes anti-reflejantes disminuyen al máximo los reflejos, evitan deslumbramientos e incrementan nuestra calidad visual. Con ello, el usuario gana comodidad, disminuyendo así la fatiga visual.</p>

            <p>Pero no todos los tratamientos anti-reflejantes son iguales. Solo los más avanzados del mercado pueden  bloquear la luz azul-violeta (la más perjudicial para la retina) y los rayos UV.</p>
            <p style="text-align: right;">Fuente: <a href="http://www.crizal.es/Anti-reflejante/Paginas/que_es_anti-reflejante.aspx" target="_blank"> CRIZAL</a></p>
            </div>
                  
          <div class="col-md-4"  style="padding-left: 30px; padding-right: -40px; text-align: justify;">
                <p><h4 style="text-align: center;"><b>Fotocromático</b></h4>
                <img src="/images/CF.jpg"></p>
                <p>Estos lentes especiales se oscurecen a la luz del sol y se aclaran a la sombra.</p>

                <p>Los lentes fotocromáticos se oscurecen a través de diversos procesos, según el tipo de lente y según la marca. La mayoría de los lentes fotocromáticos utilizan tintes exclusivos de cada fabricante que tienen cambios químicos que los oscurecen al estar expuestos a la luz UV.</p>

            <p>Ahora los lentes fotocromáticos vienen en muchos materiales y en una gama de colores. La marca más popular hoy es Transitions, pero los lentes fotocromáticos son de muchas marcas, entre ellas Sensity, Thin & Dark y PhotoFusion.</p>
            <br>
            <p style="text-align: right;">Fuente: <a href="https://www.aao.org/salud-ocular/anteojos-lentes-de-contacto/ventajas-y-desventajas-de-los-lentes-fotocromatico" target="_blank"> American Academy of Ophthalmology</a></p>
            </div>

            <div class="col-md-4"  style="padding-left: 30px; padding-right: -40px; text-align: justify;">
                <p><h4 style="text-align: center;"><b>Lentes de sol</b></h4>
                <img src="/images/GS.jpg"></p>
                
            <p>Los lentes de sol sirven para proteger a sus ojos de los daños causados por el sol, y deben ser usados en:</p>
            <p> Cuando se esté al aire libre, durante el verano, cuando esté en la playa, cuando se participe en deportes de invierno, especialmente a altitudes altas, cuando se usen medicamentos que puedan causar sensibilidad a la luz (fotosensibilidad).</p>
            <p>Los lentes de sol no pueden proteger sus ojos contra ciertas fuentes de luz intensa que pueden causar daños severos a sus ojos: La soldadura de arco, las camas o luces de bronceado, un campo de nieve, o mirar directamente al sol, especialmente durante un eclipse solar.</p>
            <p style="text-align: right;">Fuente: <a href="https://www.aao.org/salud-ocular/anteojos-lentes-de-contacto/anteojos-de-sol" target="_blank"> American Academy of Ophthalmology</a></p>
            </div>
          </div>
        <a href="#myPage" title="Índice">
          <span class="glyphicon glyphicon-chevron-up"></span>
        </a>
      </div>
    </div>

    <hr id="materiales-de-armazon">
    <div class="col-md-13">
      <div class="panel">
        <div class="panel-heading" style="text-align: left; padding-left: 30px; padding-left: 80px;"><h3> Materiales de armazón</h3></div>
                <div class="panel-body" style=" padding-left: 50px; padding-right: 50px;">
          <div class="col-md-6"  style="padding-left: 30px; padding-right: -40px; text-align: left;">
                <p><h4><b>Metálicos</b></h4> 
                <img src="/images/metalicos.jpg"></p>
                <b>TITANIO</b>
            <p>El titanio es un metal muy ligero, hipoalergénico, flexible, muy resistente y anticorrosivo.  Los armazones hechos de titanio son muy cómodos, generalmente son  armazones delgados, es muy usado para armazones de tres piezas, y son muy duraderos.</p>

            <b>BETA-TITANIO</b>
            <p>Resulta de hacer una aleación de titanio con otros metales. Esta variedad se usa para hacer armazones de titanio flexible, con lo que se obtienen armazones aún más duraderos. La desventaja es que su ajuste es muy difícil.</p>

            <b>ACERO INOXIDABLE</b>
            <p>Los armazones de este material son livianos, resistentes, hipoalergénicos (si en su aleación no tienen níquel). Se pueden obtener muchas formas de armazones y se puede combinar con el acetato de celulosa para obtener más variedad en los diseños. Son resistentes a la corrosión, abrasión y el calor.</p>

            <b>MONEL</b>
            <p>Se obtiene de la aleación de varios metales, los armazones de este material son ligeros y muy maleables, lo que facilita mucho su ajuste en caso de que quien los vaya a usar necesite un ajuste especial. La desventaja que presentan es que en ocasiones puede haber reacciones con la piel.</p>

            </div>
                  
          <div class="col-md-6"  style="padding-left: 30px; padding-right: -40px; text-align: left;">
                  <p><h4><b>Plástico y otros materiales</b></h4>
                  <img src="/images/acetato.jpg"></p>
                  <b>ACETATO DE CELULOSA</b>
            <p>Es un material plástico obtenido a base de compuestos vegetales como el algodón. Es hipoalergénico, los armazones son generalmente gruesos, se puede combinar con materiales metálicos, no se despintan ya que el color está inmerso en el material, no tienen plaquetas por lo que suelen ser más cómodos para la nariz. Se recomiendan para graduaciones altas ya que por su grosor esconden las micas gruesas.</p>

            <b>OPTYL</b>
            <p>Es una resina hipoalergénica. Las gafas hechas en este material son, en general, translucidas aunque algunas monturas pueden ser igualmente opacas. Las principales ventajas son su solidez, su ligereza (más ligero incluso que el acetato de celulosa), sus propiedades ininflamables y el hecho de que una vez caliente, el material adquiere automáticamente su forma inicial.</p>

            <b>FIBRA DE CARBONO</b>
            <p>Pesa casi la mitad de lo que pesa el acero inoxidable y mucho más resistente.  Su único inconveniente es la escasa posibilidad de coloración. Los diseñadores deben privilegiar más la forma que el color. Los armazones de este material son muy cómodos por su ligereza. Muy recomendada para personas que no soportan peso sobre su nariz.</p>

            <b>MADERA</b>
            <p>Este material se ha puesto de moda desde el año pasado. Con la madera se consiguen armazones muy originales ya que es prácticamente imposible lograr hacer un armazón igual al otro. Son cómodas, pero su peso dependerá del tipo de madera que sea empleado. Al igual que las de acetato de celulosa se recomiendan para graduaciones altas. Tienen la desventaja de ser muy frágiles, por ello deben ser manejadas con mucho cuidado, tanto por el usuario como por el personal de óptica y laboratorio óptico.</p>
            <p style="text-align: right;">Fuente: <a href="https://www.cuidomivista.com.mx/2014/07/03/materiales-de-armazones-ópticos/" target="_blank"> Cuido mi Vista</a></p>
            </div>
          </div>
              <a href="#myPage" title="Índice">
                <span class="glyphicon glyphicon-chevron-up"></span>
              </a>
      </div>
    </div>

    <hr id="mandar-receta">
    <div class="col-md-13">
      <div class="panel">
        <div class="panel-heading" style="text-align: left; padding-left: 30px; padding-left: 80px;"><h3> ¿Cómo puedo mandar mi receta?</h3></div>
                <div class="panel-body" style=" padding-left: 50px; padding-right: 50px;">
                  <div class="col-md-6"  style="padding-left: 30px; padding-right: -40px; text-align: left;"> 
                    <b></b>
                    <p>En un mesage via WhatsApp envía una imagen legible de tu receta anexando en este los siguientes datos:</p>
                    <ul>
                      <li>Fecha de compra</li>
                      <li>Nombre de Usiario</li>
                      <li>Total de la compra</li>
                      <li>Articulo(s) comprados</li>
                    </ul>

                    <b></b>
                    <p>Si compras más de un armazón, no olvides especificar a que armazón se le aplicará la graduación.</p>
                    <p><b>¿Puedo pedir mis armazones sin graduación?</b></p> 
                    <p>Sí. Si compras más de un armazón y uno lo quieres sin gradiación, envía un mensaje via WhatsApp con los datos que se especificaron anteriormente y especifica que armazón quieres sin graduación. Si no se envía una receta médica, de manera automática se eviarán tus armazones con un lente transparente sin graduación.</p>
                  </div>
  
                  <div class="col-md-6"  style="padding-left: 30px; padding-right: -40px; text-align: left;">
                    <img src="/images/whatsapp.jpg">
                  </div>
                </div>

                  <a href="#myPage" title="Índice">
                    <span class="glyphicon glyphicon-chevron-up"></span>
                  </a>
      </div>
    </div>

    <hr id="garantia-armazones">
    <div class="col-md-13">
      <div class="panel">
        <div class="panel-heading" style="text-align: left; padding-left: 30px; padding-left: 80px;"><h3> ¿Tienen garantía mis armazones?</h3></div>
                <div class="panel-body" style=" padding-left: 50px; padding-right: 50px;">
          <div class="col-md-6"  style="padding-left: 30px; padding-right: -40px; text-align: left;">
            <b></b>
                    <p>Sí, todos nuestros armazones cuentan con garantía y si ocurre algún daño por defecto de fábrica puedes enviarlo por paquetería o acudiendo a nuesrtra sucursal para hacer valida la garantia y cambiarle su armazón por uno nuevo (del mismo modelo) totalmente gratis.</p>
                    <p><b>NOTA</b></p> 
                    <p>La garantía puede variar de tiempo de acuerdo al material y a la marca de su armazón.</p>
                      
            </div>
                  
          <div class="col-md-6"  style="padding-left: 30px; padding-right: -40px; text-align: left;">
                  <img src="/images/garantia.png">
            </div>
          </div>
            <a href="#myPage" title="Índice">
              <span class="glyphicon glyphicon-chevron-up"></span>
            </a>
      </div>
    </div>

    <hr id="como-comprar-en-tienda">
    <div class="col-md-13">
      <div class="panel">
        <div class="panel-heading" style="text-align: left; padding-left: 30px; padding-left: 80px;"><h3> ¿Cómo comprar en la tienda en línea?</h3></div>
                <div class="panel-body" style=" padding-left: 50px; padding-right: 50px;">
          <div class="col-md-6"  style="padding-left: 30px; padding-right: -40px; text-align: left;">
                      
            </div>
                  
          <div class="col-md-6"  style="padding-left: 30px; padding-right: -40px; text-align: left;">
                  
            </div>
          </div>  
          <a href="#myPage" title="Índice">
            <span class="glyphicon glyphicon-chevron-up"></span>
          </a>
      </div>
    </div>

  </div>

</div> 
</body>          

<!--<p><a href=""><i class="fa fa-caret-right"></i> </a></p>-->
@stop
