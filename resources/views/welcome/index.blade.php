
@extends('layout.application')

@section('content')

@include('partials.slider')
<head>
	<style type="text/css">
		.main-title{
			color: #19398b !important;
			font-size: 23px;
		}
	</style>
</head>
	<div class="container text-center">
		
		
		<br>
		<br>
		<div id="products">
			@foreach ($products as $product)
				<div class="product white-panel" style="height: 496px !important;">
					<div class="panel-heading">
						<h3>{{ $product->name }}</h3>
					</div>

					<img src="{{ asset('images/products/' . $product->image) }}" class="img-responsive img-shirt">
					<div class="product-info">
						<p>{{ $product->extract }}</p>
						<h3><span class="label label-success">Precio: ${{ $product->price }}</span></h3>
						<p>
							<a href="{{ route('cart.add', $product->slug) }}" class="btn btn-warning"><i class="fa fa-cart-plus"></i> Comprar</a>
							<a href="{{ route('products.show', $product->slug) }}" class="btn btn-primary"> <i class="fa fa-chevron-circle-right"></i> Detalles</a>
						</p>
					</div>

				</div>
			@endforeach
		</div>
	</div>
@stop