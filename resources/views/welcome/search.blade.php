@extends('layout.application')



@section('content')



<div class="container">
  <div class="text-center">

  <div class="page-header">
      <h1><i class="fa fa-search"></i> Resultado de la búsqueda: {{$search}}</h1>
  </div>

  <h1></h1>

  @if (isset($message))
  <div class='bg-warning' style='padding: 20px'>
      {{$message}}
  </div>
  <br>
  <a href="{{ route('welcome.index') }}" class="btn btn-primary">
            <i class="fa fa-chevron-circle-left"></i> Ver todos los productos
          </a>

  @endif
  <hr>
    @foreach ($products as $product)
          <div class="product white-panel" style="height: 496px !important;">
            <h3>{{ $product->name }}</h3>
            <img src="{{ asset('images/products/' . $product->image) }}" class="img-responsive img-shirt">
            <div class="product-info">
              <p>{{ $product->extract }}</p>
              <h3><span class="label label-success">Precio: ${{ $product->price }}</span></h3>
              <p>
                <a href="{{ route('cart.add', $product->slug) }}" class="btn btn-lg btn-block btn-outline-primary"><i class="fa fa-cart-plus"></i> La quiero</a>
                <a href="{{ route('products.show', $product->slug) }}" class="btn btn-primary"> <i class="fa fa-chevron-circle-right"></i> Leer mas</a>
              </p>
            </div>
          </div>
        @endforeach
      </div>
</div>
</div>
@stop
