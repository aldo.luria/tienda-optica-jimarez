@extends('layout.application')

@section('content')
	<div class="container text-center">
		<div class="page-header">
			<h1><i class="fa fa-shopping-cart"></i> Detalle del producto </h1>
		</div>
		<div class="row no-margin">
			<div class="col-md-6">
				<div class="product-block">
					<img src="{{ asset('images/products/' . $product->image) }}" class="img-responsive">
				</div>
			</div>
			<div class="col-md-6">
				<div class="product-block panel large-padding" style="padding: 10px 15px;">
					<h3>{{ $product->name }}</h3>
					<div class="product-info">
						<p>{{ $product->description }}</p>
						<h3>
							<span class="label label-success">Precio: ${{ $product->price }}</span>
						</h3>
						<p>
							<a href="{{ route('cart.add', $product->slug) }}" class="btn btn-warning btn-block"><i class="fa fa-cart-plus"></i> Comprar</a>
						</p>
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="product-block panel large-padding" style="padding: 10px 15px;">
					<h4>Detalles de producto</h4>
					<div class="product-info">
						<table class="table table-striped table-hover table-bordered">
							<tr><th class="text-center" width='30%'>Marca: </th><td>{{ $product->marca }}</td></tr>
							<tr><th class="text-center">Color: </th><td>{{ $product->color }}</td></tr>
							<tr><th class="text-center">Material: </th><td>{{ $product->material }}</td></tr>
							<tr><th class="text-center">Tipo: </th><td>{{ $product->category->name }}</td></tr>
						</table>
					</div>
				</div>
			</div>

		</div><br><br>
		<p>
			<a href="{{ route('welcome.index') }}" class="btn btn-primary">
				<i class="fa fa-chevron-circle-left"></i> Regresar
			</a>
		</p><br>
	</div>
@stop