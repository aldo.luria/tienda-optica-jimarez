<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     	 $data = array(
			[
				'name' => 'Armazón',
				'slug' => 'armazon',
				'description' => '¡Escoge el armazón que mas te agrade! Puedes pedirlos con o si graduación',
			],
			[
				'name' => 'Lentes de contacto',
				'slug' => 'lentes-contacto',
				'description' => '¡Escoge los lentes de contacto que te gusten más!',
			],
			[
				'name' => 'Accesorios',
				'slug' => 'accesorios',
				'description' => '¡Escoge los accesorios que necesites y más te gusten!',
			]
		);
		Category::insert($data);
    }
}
