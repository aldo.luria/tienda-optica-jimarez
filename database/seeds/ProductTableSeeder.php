<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = array(
			[
				'name' 			=> 'Armazón de gafa',
				'slug' 			=> 'armazon-1',
				'description' 	=> 'Tamaño: cm.',
				'extract' 		=> 'Tamaño.',
				'marca' 		=> 'Ray Ban.',
				'price' 		=> 2750.00,
				'color' 		=> 'Rojo.',
				'material' 		=> 'Titanio.',
				'image' 		=> 'tiendus_1522042627.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'Armazón',
				'slug' 			=> 'armazon-2',
				'description' 	=> 'Tamaño: cm.',
				'extract' 		=> 'Tamaño.',
				'marca' 		=> 'Burberry',
				'price' 		=> 3050.00,
				'color' 		=> 'Negro',
				'material' 		=> 'Acetato',
				'image' 		=> 'tiendus_1522042662.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'Armazón',
				'slug' 			=> 'armazon-3',
				'description' 	=> 'Tamaño: cm',
				'extract' 		=> 'Tamaño',
				'marca' 		=> 'Ray Ban',
				'price' 		=> 2000.00,
				'color' 		=> 'Negro',
				'material' 		=> 'Acetato',
				'image' 		=> 'tiendus_1522042649.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 1
			],
			[
				'name' 			=> 'Estuche',
				'slug' 			=> 'estuche-1',
				'description' 	=> 'Tamaño: cm',
				'exstract' 		=> 'Tamaño',
				'marca' 		=> 'Ray Ban',
				'price' 		=> 700.00,
				'color' 		=> 'Negro',
				'material' 		=> 'Rigido',
				'image' 		=> 'tiendus_1522042545.jpg',
				'visible' 		=> 1,
				'created_at' 	=> new DateTime,
				'updated_at' 	=> new DateTime,
				'category_id' 	=> 3
			],
				
		);
		Product::insert($data);
    }
}
