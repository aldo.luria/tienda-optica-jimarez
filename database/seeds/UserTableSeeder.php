<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
   ;


		 $data = array(
			[
				'name' 		=> 'Mauricio', 
				'last_name' => 'Jimarez', 
				'email' 	=> 'consultoriosopticos_tlax@hotmail.com', 
				'username' 	=> 'MauricioJimaRez',
				'password' 	=> \Hash::make('123456'),
				'type' 		=> 'admin',
				'active' 	=> 1,
				'address' 	=> 'Tlaxcala, Tlaxcala',
				'created_at'=> new DateTime,
				'updated_at'=> new DateTime
			],
			[
				'name' 		=> 'Aldo', 
				'last_name' => 'Luria', 
				'email' 	=> 'aldo.luria@gmail.com', 
				'username' 	=> 'aldoluria',
				'password' 	=> \Hash::make('123456'),
				'type' 		=> 'normal',
				'active' 	=> 1,
				'address' 	=> 'Tlaxcala, Tlaxcala',
				'created_at'=> new DateTime,
				'updated_at'=> new DateTime
			],

		);
		User::insert($data);
    }
}
