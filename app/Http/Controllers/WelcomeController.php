<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input; 
use App\Product;
use App\Category;

class WelcomeController extends Controller
{
	public function index(){
		$products = Product::all();
		return view('welcome.index')->with('products', $products);
	}
	/*
	public function show($slug){
		$product = Product::findBySlugOrFail($slug);
		return view('welcome.show-product')->with('product', $product);
	}

*/
    Public function show($slug)
    {
    	$product =Product::where('slug',$slug)->first();
    	//dd($product);

    	return view('welcome.show-product', compact('product'));

    }



	public function searchCategory($slug){
		$category = Category::findBySlugOrFail($slug);
		$products = $category->products()->paginate(4);
		$products->each(function($products){
			$products->category;
		});
		return view('welcome.index')->with('products', $products);
	}


    public function search($search){
        $search = urldecode($search);
        $products = Product::select()
                ->where('name', 'LIKE', '%'.$search.'%')
                ->orderBy('id', 'desc')
                ->get();
        
        if (count($products) == 0){
            return View('welcome.search')
            ->with('message', 'No hay resultados que mostrar')
            ->with('products', $products)
            ->with('search', $search);
        } else{
            return View('welcome.index')
            ->with('products', $products)
            ->with('search', $search);
        }
    }


    
	 Public function ayuda()
    {
    	
    	return view('ayuda');

    }
     Public function conocenos()
    {
    	
    	return view('conocenos');

    }

    Public function terminos()
    {
    	
    	return view('terminos');

    }

    Public function politicas()
    {
    	
    	return view('politicas');

    }
}
