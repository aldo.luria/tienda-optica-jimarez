<?php
return array(
    // set your paypal credential
    'client_id' => 'AQJPTnDrA3p446yAF3P_lkhDHUv3FcH9fsdTBX5sM6BVWc_C9CGA4lF9wBWDhvLDTw2ccgHiBg72idDo',
    'secret' => 'EDISEOUfLqqaVljzsR8TVMr9W0hDehcsj5FIvJy9CY96kRGhqJCuLUxVmOyMmNFkTENSHd9Yvn-1J3iM',

    /**
     * SDK configuration 
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'sandbox',

        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 30,

        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,

        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',
        
        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);